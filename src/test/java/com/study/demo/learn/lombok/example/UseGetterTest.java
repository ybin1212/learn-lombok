package com.study.demo.learn.lombok.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author yangbin
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class UseGetterTest {

    @Test
    public void testSetter() {

        // @Getter：注解在类上, 为所有属性添加 get方法、注解在属性上为该属性提供 get 方法.

        UseGetter example = new UseGetter();

        example.getId();
        example.getName();
    }
}