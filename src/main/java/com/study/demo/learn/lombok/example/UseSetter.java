package com.study.demo.learn.lombok.example;

import lombok.Setter;

/**
 * @author yangbin
 **/
@Setter
public class UseSetter {

    private Long id;

    @Setter
    private String name;
}
