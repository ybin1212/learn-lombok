package com.study.demo.learn.lombok.example;

import lombok.extern.apachecommons.CommonsLog;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;

/**
 * @author yangbin
 **/
public class UseLogger {
    @Log
    static class UseLog {
        public static void log() {
            log.info("【@Log】");
        }
    }
    @Log4j2
    static class UseLog4j2 {
        public static void log() {
            log.info("【@Log4j2】");
        }
    }
    @Slf4j
    static class UseSlf4j {
        public static void log() {
            log.info("【@Slf4j】");
        }
    }
    @CommonsLog
    static class UseCommonsLog {
        public static void log() {
            log.info("【@CommonsLog】");
        }
    }
    // @Log4j
    // @XSlf4j
    // @JBossLog
}
