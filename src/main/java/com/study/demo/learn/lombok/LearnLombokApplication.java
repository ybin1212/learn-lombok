package com.study.demo.learn.lombok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnLombokApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnLombokApplication.class, args);
	}
}
