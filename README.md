# Lombok 的使用

[Lombok](https://projectlombok.org/) 是一种 Java 实用工具, 用来简化 `getter` and `setter` 等等.

## 注解使用

```
@Data
    注解在 类 上, 将类提供的所有属性都添加 get、set 方法, 并添加 equals、canEqual、hashCode、toString方法.

@Setter
    注解在 类 上, 为所有属性添加 set 方法、注解在属性上为该属性提供 set 方法.

@Getter
    注解在 类 上, 为所有的属性添加 get 方法、注解在属性上为该属性提供 get 方法.

@NotNull
    注解在 属性 上, 会自动产生一个关于此参数的非空检查, 如果参数为空, 则抛出一个空指针异常, 也会有一个默认的无参构造方法.

@Log / @Log4j2 / @Slf4j / @CommonsLog / @Log4j / @XSlf4j / @JBossLog
    注解在 类 上, 为类提供一个属性名为 log 日志对象, 提供默认构造方法.

@Builder
    使用 builder 模式创建对象.

@NoArgsConstructor
    为类提供一个无参的构造方法.

@AllArgsConstructor
    为类提供一个全参的构造方法, 加了这个注解后, 类中不提供默认构造方法了.

@ToString
    创建一个 toString 方法.

@Accessors(chain = true)
    使用链式设置属性, set 方法返回的是 this 对象.

@RequiredArgsConstructor
    这个注解用在 类 上, 使用类中所有带有 @NonNull 注解的或者带有 final 修饰的成员变量生成对应的构造方法.

@UtilityClass
    工具类.

@ExtensionMethod
    设置父类.

@FieldDefaults
    设置属性的使用范围, 如 private、public 等, 也可以设置属性是否被 final 修饰.

@Cleanup
    这个注解用在 变量 前面, 可以保证此变量代表的资源会被自动关闭, 默认是调用资源的 close() 方法,
    如果该资源有其它关闭方法, 可使用 @Cleanup("methodName") 来指定要调用的方法, 也会生成默认的构造方法.

@EqualsAndHashCode
    注解在 类 上, 可以生成 equals、canEqual、hashCode 方法.

@toString
    这个注解用在 类 上,可以生成所有参数的 toString 方法, 还会生成默认的构造方法.

@Value
    这个注解用在 类 上, 会生成含所有参数的构造方法, get 方法, 此外还提供了 equals、hashCode、toString 方法.
    
@SneakyThrows
    这个注解用在 方法 上, 可以将方法中的代码用 try-catch 语句包裹起来, 捕获异常并在 catch 中用 Lombok.sneakyThrow(e) 把异常抛出,
    可以使用 @SneakyThrows(Exception.class) 的形式指定抛出哪种异常, 也会生成默认的构造方法.

@Synchronized
    这个注解用在 类方法 或者 实例方法 上, 效果和 synchronized 关键字相同, 区别在于锁对象不同, 对于类方法和实例方法, 
    synchronized 关键字的锁对象分别是类的 class 对象和 this 对象, 而 @Synchronized 的锁对象分别是 私有静态 final 对象 lock 和 私有 final 对象 lock, 
    当然, 也可以自己指定锁对象, 此外也提供默认的构造方法.
```